#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use List::Util 'first';

require './PodWEB.pm';
PodWEB->import();
our ($VERSION, %query, %config, %data, $output, $cache_file, @id_list, $search,
	 $t);

my @special =
(
	[ 'lower', qr/^(?!perl)[a-z]/, 'a-z'   ],
	[ 'other', qr/^[^a-zA-Z]/,     'other' ],
);

render (sub {
	$data{module} = 'Module Browser';
	$data{module_version} = '';
	unless ($config{'search.index'})
	{
		$data{content} =
			 "<h1>Browser not available</h1>\n" .
			 "<p>Index generation required by module browser is turned off.\n";
		return;
	}
	my @names = sort { $a->[0] cmp $b->[0] } @{(pod_fts_cache (''))[0]};

	my ($i, $p, $n, $sm) = 0;

	$data{content} .=
		 "\n<div class=\"browser\">\n" .
		 "	<a href=\"" . encode_html (query_make (p => 'perl')) . "\">perl</a>\n";
	foreach my $s (@special)
	{
		$data{content} .=
			 '	<span class="separator"> • 	</span>' .
			 '<a href="' . encode_html (query_make (p => $s->[0])) . '">' .
			 $s->[2] . "</a>\n"
			 if first { $_->[0] =~ $s->[1] } @names;
	}

	$n = '';
	foreach (map { substr ($_->[0], 0, 1) } @names)
	{
		next if m/[^A-Z]/ or $_ eq $n;
		$n = $_;
		$data{content} .=
			 '	<span class="separator"> • 	</span>' .
			 '<a href="' . encode_html (query_make (p => $n)) . '">' .
			 "$n</a>\n";
	}

	$data{content} .= "</div>\n";

	$data{content} .= "\n" . '<ul class="browser">' . "\n";
	$query{p} //= 'perl';
	$p = first { $_->[0] eq $query{p} } @special;
	$p = $p ? $p->[1] : qr/^\Q$query{p}\E/;
	while ($i < @names)
	{
		$n = $names[$i++];
		next unless $n->[0] =~ $p;
		$data{content} .=
			 "	<li>\n" . '		<a href=".' .
			 encode_html (query_make (s => $n->[0])) . '">' .
			 encode_html ($n->[0]) . '</a>' .
			 ($n->[1] ? " – $n->[1]" : '') .
			 "\n";
		$sm = '';
		while ($i < @names and
			 substr ($names[$i][0], 0, length ($n->[0]) + 2) eq "$n->[0]::")
		{
			$sm .= '		<a style="margin-right: 2ex;" href=".' .
				 encode_html (query_make (s => $names[$i][0])) .
				 '" title="' . $names[$i][1] . '">' .
				 encode_html ($names[$i][0]) . "</a>\n";
			$i++;
		}
		$data{content} .= "		<br>\n$sm" if $sm;
	}
	$data{content} .= "</ul>\n\n";
});
