package PodWEB;

use v5.20;

use strict;
use warnings;
no warnings 'syntax';
no warnings 'redundant';
use utf8;

our $VERSION = '1.5';
our (%query, %config, %data, $output, $cache_file, $cache_dir, @id_list,
	 $search, $t);

use parent 'Exporter';
our @EXPORT = qw/$VERSION %query %config %data $output $cache_file $cache_dir
	 @id_list $search $t fail redirect read_file encode_html encode_url
	 encode_filename query_make pod_text pod_text_markup pod_id_tidy pod_id
	 pod_index_scan pod_render pod_search pod_all_modules pod_fts_refresh
	 pod_fts_cache pod_index_cache render/;

use Scalar::Util qw/looks_like_number/;
use POSIX qw/strftime/;
use List::Util qw/first all/;
use Time::HiRes;
use Encode qw/encode/;
use Pod::Simple::SimpleTree;
use Fcntl qw/:flock :seek/;

binmode (*STDOUT, ':utf8');
binmode (*STDERR, ':utf8');

$t = Time::HiRes::time();
my $inc;

sub fail
{
	print (
		 "Content-type: text/plain; charset=utf-8\r\n" .
		 "\r\n" .
		 "\n@_\n");
	exit (0);
}

sub redirect
{
	print (
		 "Status: 302 Found\r\n" .
		 "Location: @_\r\n" .
		 "Content-type: text/plain; charset=utf-8\r\n" .
		 "\r\n");
	exit (0);
}



{
	my ($f, $key, $value);
	if (open ($f, '<', 'config'))
	{
		while (readline ($f))
		{
			next unless m/^\s*([a-z]+(?:\.[^=]+)*)=\s*(.*?)\s*\r?$/;
			($key, $value) = ($1, $2);
			$key =~ s/\s*$//s;
			fail ("The value of $key is supposed to be a positive integer")
				 if $key =~ m/\.(max|num)$/ and $value !~ m/^[1-9][0-9]*$/;
			$config{$key} = $value;
		}
		close ($f);
	}
}



%query = map { (split ('=', $_))[0..1] } split ('&', $ENV{QUERY_STRING})
	 if defined $ENV{QUERY_STRING};
foreach (keys %query)
{
	$query{$_} //= '';
	fail ("Variable maximum length exceeded")
		 if length $query{$_} > ($config{'query.length.max'} // 128);
	$query{$_} =~ s/\+/ /gs;
	$query{$_} =~ s/%([A-F0-9]{2})/chr hex $1/egs;
	utf8::decode ($query{$_});
}



%data = %query;
$data{version} = $^V;
$data{title} = '';



sub read_file
{
	my ($name, $dest) = @_;
	my $f;
	open ($f, '<:utf8', $name) or die "Cannot open '$name': $!\n";
	read ($f, $$dest, 104857600);
	close ($f);
}

sub encode_html
{
	my $s = "@_";
	$s =~ s/&/&amp;/gs;
	$s =~ s/</&lt;/gs;
	$s =~ s/>/&gt;/gs;
	$s =~ s/"/&quot;/gs;
	$s =~ s/'/&apos;/gs;
	$s =~ s/\n/<br>/gs;
	return $s;
}

sub encode_url
{
	my $s = "@_";
	utf8::encode ($s);
	$s =~ s/([^a-zA-Z0-9 ])/sprintf ('%%%02X', ord $1)/egs;
	$s =~ s/ /+/gs;
	return $s;
}

sub encode_filename
{
	my $s = "@_";
	$s =~ s/\s+/_/gs;
	utf8::encode ($s);
	$s =~ s/([^a-zA-Z0-9_.,@:])/sprintf ('%02X', ord $1)/egs;
	return $s;
}

sub query_make
{
	my %set = @_;
	my $q = '';
	foreach (keys %query)
	{
		next if exists $set{$_};
		$q .= ($q ? '&' : '?') . encode_url ($_) . '=' . encode_url ($query{$_});
	}
	$q .= ($q ? '&' : '?') . "$_=$set{$_}" foreach (keys %set);
	return $q;
}



my $perl_version = length ($query{v} // '') ? $query{v} : 'default';
$data{options} = '';
foreach (sort keys %config)
{
	next unless substr ($_, 0, 5) eq 'path.';
	my $label = substr ($_, 5);
	my @path = split (':', $config{$_});
	my $id = $label =~ s/[^a-zA-Z0-9_.]//grs;
	my $selected = '';
	if ($perl_version eq $id)
	{
		$selected = ' selected';
		$inc = \@path;
	}
	$data{options} .= "<option value=\"$id\"$selected>" . encode_html ($label) .
		 "</option>" if $label ne 'default';
}
unless ($inc)
{
	$inc = \@INC;
	$perl_version = 'default';
	$query{v} = '';
}



my @elements =
	 (
		 { name => 'Document', multiline => 1 },
		 { name => 'I', start => '<i>', end => '</i>' },
		 { name => 'B', start => '<b>', end => '</b>' },
		 { name => 'C', start => '<code>', end => '</code>' },
		 { name => 'F', start => '<code class="filename">', end => '</code>' },
		 { name => 'S', start => '<span style="white-space: nowrap;">', end => '</span>' },
		 { name => 'X', nonprintable => 1 },
		 { name => 'over-bullet', start => '<ul>', end => '</ul>', block => 1, multiline => 1, linebreaks => 1, },
		 { name => 'item-bullet', start => '<li>', block => 1 },
		 { name => 'over-number', start => '<ol>', end => '</ol>', block => 1, multiline => 1, linebreaks => 1, },
		 { name => 'item-number', start => '<li>', block => 1 },
		 { name => 'over-text', start => '<dl>', end => '</dl>', block => 1, multiline => 1, linebreaks => 1, },
		 { name => 'over-block', start => '<div class="indent">', end => '</div>', block => 1, multiline => 1, },
	 );

sub pod_text_r
{
	my ($element, $text, $sub) = @_;
	my $name = $element->[0];
	my $params = $element->[1];
	return if $name eq 'for';
	if ($sub)
	{
		my $e = first { $_->{name} eq $name } @elements if length $name;
		return if $e and $e->{nonprintable};
	}
	$$text .= ' ' unless length $name == 1;
	foreach (@$element[2..$#$element])
	{
		if (ref $_ eq 'ARRAY')
		{
			pod_text_r ($_, $text, 1);
		}
		else
		{
			$$text .= $_;
		}
	}
}

sub pod_text
{
	my $text = '';
	pod_text_r (shift, \$text);
	$text =~ s/^[\r\n\s]*//s;
	$text =~ s/[\r\n\s]*$//s;
	return $text;
}

sub pod_text_markup_r
{
	my ($element, $text, $sub) = @_;
	my $name = $element->[0];
	my $params = $element->[1];
	return if $name eq 'for' or $name eq 'Verbatim';
	my $e = first { $_->{name} eq $name } @elements if length $name;
	return if $e->{block};
	return if $sub and $e and $e->{nonprintable};
	$$text .= '<br>' if length ($$text) and $name eq 'Para';
	$$text .= $e->{start} if $e and $e->{start};
	foreach (@$element[2..$#$element])
	{
		if (ref $_ eq 'ARRAY')
		{
			pod_text_markup_r ($_, $text, 1);
		}
		else
		{
			$$text .= encode_html ($_);
		}
	}
	$$text .= $e->{end} if $e and $e->{end};
}

sub pod_text_markup
{
	my $markup = '';
	pod_text_markup_r (shift, \$markup);
	return $markup;
}

sub pod_abstract
{
	my $root = shift;
	my $abstract = '';
	my $sn = 0;
	foreach (@$root[2..$#$root])
	{
		if (ref $_ eq 'ARRAY' and $_->[0] =~ m/^head/)
		{
			last if $sn;
			$sn = ($_->[0] eq 'head1' and defined $_->[2] and $_->[2] eq 'NAME');
			next;
		}
		next unless $sn;
		$abstract .= '<br>' if length ($abstract);
		$abstract .= pod_text_markup ($_);
	}
	return $abstract;
}

sub pod_id_tidy
{
	my $id = shift;
	$id =~ s/^[\r\n\s]*//s;
	$id =~ s/[\r\n\s]*$//s;
	$id =~ s/[\r\n\s]+/./gs;
	$id =~ s/([^a-zA-Z0-9_.-])/sprintf ('%02x', ord $1)/egs;
	$id = "A$id" if $id !~ m/^[a-zA-Z]/;
	return substr ($id, 0, 32);
}

sub pod_id
{
	my ($element, $level, $id_list) = @_;
	state $id_cnt = 0;
	my $id = pod_id_tidy (pod_text ($element));
	if (length $id and not first { $_->{id} eq $id } @$id_list)
	{
		push (@$id_list, { id => $id, text => pod_text_markup ($element),
			 level => $level, });
		return $id;
	}
	return 'C' . $id_cnt++;
}

sub pod_index_scan_r
{
	my ($element, $index, $section, $id_list) = @_;
	my $name = $element->[0];
	my $params = $element->[1];
	return if $name eq 'for';
	if ($name eq 'X')
	{
		push (@$index, [pod_id ($element, 0, $id_list), pod_text_markup ($element),
			 $$section, pod_text ($element)]);
		return;
	}
	elsif ($name eq 'head1')
	{
		$$section = pod_text ($element);
	}

	foreach (@$element[2..$#$element])
	{
		pod_index_scan_r ($_, $index, $section, $id_list) if (ref $_ eq 'ARRAY');
	}
}

sub pod_index_scan
{
	my ($element, $id_list) = @_;
	my @index;
	my $section = '';
	pod_index_scan_r ($element, \@index, \$section, $id_list);
	return \@index;
}

sub pod_verbatim
{
	my ($text, $indent, $syntax) = @_;

	if (not $syntax and $text =~ m/^(\s*)(-[+-]+)\s*$/m)
	{
		my ($tindent, $tilen, $ruler, $rlen) =
			 ($1, length ($1), $2, length ($2) - 1);
		my @lines = split ("\n", $text);
		my $ln = 0;
		goto TABLE_SKIP unless all {
				 $ln++,
				 m/^$tindent-[+-]{$rlen}\s*$/s or
				 m/^$tindent .{1,$rlen}\s*$/ or
				 ($ln >= @lines - 1 and m/^$tindent\([0-9]+ rows\)$/) or
				 ($ln == @lines and m/^${tindent}Time:\s*[0-9.a-z\s]+$/)
			 } @lines;

		my @table;
		my @cl = map { length ($_) } split (/\+/, $ruler);
		my ($i, $b, $footer) = (0, 0, '');
		while ($i < @lines)
		{
			my $l = substr ($lines[$i++], $tilen);
			my $ll = length ($l);
			if (substr ($l, 0, 1) eq '-')
			{
				$_->{rule} = 1 foreach (@{$table[-1]});
				$b++;
				next;
			}

			if ($l =~ m/^[\(T]/)
			{
				$footer .= "$indent		<tr><td colspan=\"" . @cl . "\">$l</tr>\n";
				next;
			}

			my @cc;
			my ($p, $s, $pp, $more) = (0, 0, 0);
			foreach (my $c = 0; $c < @cl; $p++)
			{
				$p += $cl[$c++];
				$s++;
				if ($c == @cl or ($p < $ll and substr ($l, $p, 1) eq '|'))
				{
					goto TABLE_SKIP
						 unless $p >= $ll or
							 ($p < $ll and
							 substr ($l, $p - 1, 3) =~ m/^[+ ]\|\s*$/) or
							 substr ($l, $p - 1) =~ m/^[+ ]\s*$/;
					push (@cc, {text => '', start => $pp + 1, len => $p - $pp - 2,
						 block => $b, colspan => $s, rule => 0});
					while (--$s > 0)
					{
						push (@cc, {});
					}
					$pp = $p;
				}
			}
			while ()
			{
				$more = 0;
				for ($p = 0; $p < @cc; $p++)
				{
					next unless defined $cc[$p]->{start};
					last if $cc[$p]->{start} >= $ll;
					$more += ($cc[$p]->{start} + $cc[$p]->{len} < $ll and
						 substr ($l, $cc[$p]->{start} + $cc[$p]->{len}, 1) eq '+') ? 1 : 0;
					$cc[$p]->{text} .= encode_html (substr ($l, $cc[$p]->{start},
						 $cc[$p]->{len}) =~ s/^\s*(.*?)\s*$/$1/r);
				}
				last unless $more;
				$l = substr ($lines[$i++], $tilen);
				$ll = length ($l);
				$_->{text} .= '<br>' foreach (@cc);
			}
			$cc[0]->{first} = 1;
			for ($p = $#cc; $p >= 0 and not defined $cc[$p]->{block}; $p--) {}
			$cc[$p]->{last} = 1;
			push (@table, \@cc);
		}

		$_->{rule} = 1 foreach (@{$table[-1]});
		my $tc = '';
		for ($i = 0; $i < @{$table[0]}; $i++)
		{
			my $numeric = 1;
			foreach (@table)
			{
				my $c = $_->[$i];
				if (defined $c and defined $c->{block} and
					 $c->{block} and $c->{text} and
					 $c->{text} !~ /^\s*\.[0-9 ,]+\s*$/ and
					 $c->{text} !~ /^\s*[0-9 ,]+(?:\.[0-9 ]*)?\s*$/)
				{
					$numeric = 0;
					last;
				}
			}
			if ($numeric)
			{
				$_->[$i]->{numeric} = 1 foreach (@table);
			}
		}
		foreach my $tr (@table)
		{
			$tc .= "$indent		<tr>";
			$tc .= defined $_->{block}
				 ? '<' . ($_->{block} ? 'td' : 'th') . ' class="' .
					 join (' ', ($_->{first} ? 'bl' : ()), ($_->{last} ? 'br' : ()),
					 ($_->{rule} ? 'bb' : ()), ($_->{numeric} ? 'num' : '')) . '"' .
					 ($_->{colspan} > 1 ? " colspan=\"$_->{colspan}\"" : '') .
					 ">$_->{text}"
				 : ''
				 foreach (@$tr);
			$tc .= "</tr>\n";
		}

		return
			 "\n\n$indent<table>\n" .
			 ($footer ? "$indent	<tfoot>\n$footer$indent	</tfoot>\n" : "") .
			 "$indent	<tbody>\n$tc$indent	</tbody>\n" .
			 "$indent</table>\n\n";

		TABLE_SKIP:
	}

	my $tindent;
	while ($text =~ m/^( +)/gm) {
		$tindent = length($1) unless defined $tindent and $tindent < length($1);
	}
	$tindent = ' ' x ($tindent // 0);
	$text =~ s/^$tindent//gm if $tindent;

	if ($config{'syntax.engine'} and not ($syntax and $syntax eq 'none') and
		 (($syntax and $syntax =~ m/^[a-z]+$/) or $text =~ m/
			^\s*(?:
				(?:
					if|while|for|foreach|waitpid|pack|unpack|printf|select|exec|
					open|close|seek|vec
				)\s*\(|
				(?:for|foreach)\s+my[^\w]|
				(?:my|local|our|undef|delete)\s*\(?\s*[\$\%\@]|
				(?:use|require|goto|format|sub)\s|
				(?:print|printf|do|eval|exec|die|warn)(?:\s*\(|\s+\*|\s*"|\s*')|
				(?:push|pop|shift|unshift)\s*\(?\@|
				(?:print|printf|eval|BEGIN|END)\s+\{|
				(?:chomp)[\s\(;]|
				\$\w+\s*(?:=~|->)
			)/mx))
	{
		my $call = "syntax_$config{'syntax.engine'}";
		die "Invalid syntax highlighting engine '$config{'syntax.engine'}'.\n"
			 unless exists &$call;
		my $output = &{\&$call} ($syntax, $text);
		return $output if defined $output;
	}

	return "\n$indent<pre>" . encode_html ($text) . "</pre>";
}

sub pod_render
{
	state $syntax = '';
	state $section = '';
	my ($element, $c, $lvl, $id_list, $par_cnt, $linebreaks) = @_;
	my $name = $element->[0];
	my $params = $element->[1];
	my $indent = '	' x $lvl;
	my $e;

	if ($name eq 'Verbatim')
	{
		$$c .= pod_verbatim ($element->[2], $indent, $syntax);
		$syntax = '';
		return;
	}

	if ($name eq 'Para')
	{
		$e =
			 {
				 start => ($linebreaks ? ($$par_cnt ? '<br>' : '') : '<p>'),
				 block => 1,
				 multiline => 1,
			 };
		if ($section eq 'NAME' and not length $data{title})
		{
			$data{title} = pod_text ($element);
			$data{title} =~ s/[\r\n\s]+/ /s;
			$data{title} =~ s/\. *$//s;
			$data{title} = substr ($data{title}, 0, 100) . '...'
				 if length $data{title} > 100;
			$data{title} = encode_html ($data{title});
		}
		$$par_cnt++;
	}

	elsif ($name eq 'for')
	{
		if ($params->{target} eq 'syntax')
		{
			$syntax = $element->[2][2]
				 if (ref $element->[2] eq 'ARRAY' and $element->[2][0] eq 'Data');
		}
		elsif ($params->{target} eq 'html')
		{
			$$c .= "\n$indent<div>";
			foreach (@$element[2..$#$element])
			{
				$$c .= "\n" . ($_->[2] =~ s/^\s*/$indent	/gmr)
					 if ref $_ eq 'ARRAY' and $_->[0] eq 'Data' and defined $_->[2];
			}
			$$c .= "\n$indent</div>\n";
		}
		return;
	}

	elsif ($name eq 'L')
	{
		$e = {};
		if ($params->{type} eq 'pod')
		{
			my $id;
			$id = pod_id_tidy ($params->{section}) if $params->{section};
			$e->{start} =
				 '<a href="' .
				 (defined $params->{to} and length $params->{to} ?
				 encode_html (query_make (s => encode_url ($params->{to}))) : '') .
				 ($id ? "#$id" : "") .
				 '">';
			$e->{end} = '</a>';
			if ($params->{'content-implicit'} and $params->{section})
			{
				$$c .=
					 $e->{start} .
					 encode_html ($params->{section}->[2]) .
					 (($config{'link.in_to'} // 1 and $params->{to}) ?
					 encode_html (" in $params->{to}->[2]") : '') .
					 $e->{end};
				return;
			}
		}
		elsif ($params->{type} eq 'man')
		{
			unless ($params->{to} =~ m/^(.*)\(([0-9][0-9a-z]*)\)$/is)
			{
				$e->{start} =
					 "<span class=\"unknown\"><i>Invalid man reference</i></span>";
			}
			else
			{
				$e->{start} = '<a href="' .
					 sprintf ($config{'link.man'} // 'https://linux.die.net/man/%s/%s',
					 encode_url ($2), encode_url ($1)) . '">';
				$e->{end} = "</a>";
			}
		}
		elsif ($params->{type} eq 'url')
		{
			$e->{start} = '<a href="' . encode_html ($params->{to}) . '">';
			$e->{end} = '</a>';
		}
		else
		{
			$e->{start} =
				 "<span class=\"unknown\"><i>Link type '$params->{type}'</i></span>";
		}
	}

	elsif (substr ($name, 0, 4) eq 'head')
	{
		my $hl = substr ($name, 4);
		$section = pod_text ($element) if $hl eq '1';
		$e =
			 {
				 start => "<h$hl id=\"" . pod_id ($element, $hl, $id_list) . "\">",
				 end   => "</h$hl>",
				 block => 1,
			 };
	}

	elsif ($name eq 'item-text')
	{
		$e =
			 {
				 start => '<dt id="' . pod_id ($element, 0, $id_list) . '">',
				 end   => '<dd>',
				 block => 1,
			 };
		$$par_cnt = 0;
	}

	elsif ($name eq 'X')
	{
		$$c .= '<a name="' . pod_id ($element, 0, $id_list) . '"></a>';
		return;
	}

	else
	{
		$e = first { $name eq $_->{name} } @elements;
		if ($e)
		{
			return if $e->{nonprintable};
		}
		else
		{
			$e =
				 {
					 start => "<div class=\"unknown\"><i>Element $name</i><br>",
					 end   => "</div>",
					 block => 1,
				 };
		}
	}

	if ($e->{start})
	{
		$$c .= "\n$indent" if ($e->{block});
		$$c .= $e->{start};
	}

	my $cnt = $e->{block} ? 1 : 0;
	foreach (@$element[2..$#$element])
	{
		if (ref $_ eq 'ARRAY')
		{
			pod_render ($_, $c, $lvl + 1, $id_list, \$cnt, $e->{linebreaks});
		}
		else
		{
			$$c .= encode_html ($_);
		}
	}

	if ($e->{end})
	{
		$$c .= "\n$indent" if ($e->{multiline});
		$$c .= $e->{end};
	}
}

my @pod_search_list =
	 (
		 [ 'pods/', '.pod' ],
		 [ 'pods/', ''     ],
		 [ 'pod/',  '.pod' ],
		 [ 'pod/',  ''     ],
		 [ '',      '.pod' ],
		 [ '',      '.pm'  ],
	 );

sub pod_search
{
	my $search = shift;
	my $path = $search =~ s/::/\//grs;
	my ($file, $module, $module_version);

	my ($d, $s, $f, $n);
	DIR_SEARCH: foreach $d ('.', @$inc)
	{
		foreach $s (@pod_search_list, ['', ''])
		{
			$n = "$d/$s->[0]$path$s->[1]";
			if (-f $n)
			{
				if (open ($f, '<', $n))
				{
					while (readline ($f))
					{
						if (m/^=pod/ or m/^=head1/)
						{
							$file = $n;
							$module = $search;
						}
						elsif (not $module_version
							 and m/^\s*(?:(?:our)?\s*\$VERSION|\$\Q$search\E::VERSION)
							 \s*=\s*
							 ['"]([0-9A-Za-z_.-]+)['"]
							 \s*;\s*\r?$/sx)
						{
							$module_version = $1;
						}
						last DIR_SEARCH if $file and $module_version;
					}
					close ($f);
					last DIR_SEARCH if $file;
				}
			}
		}
	}

	return wantarray ? ($file, $module, $module_version) : $file;
}

sub pod_all_modules_r
{
	my ($base, $dir, $list) = @_;
	my ($d, $e, $f, $pf, $s);
	return unless opendir ($d, "$base/$dir");
	foreach my $n (readdir ($d))
	{
		next if $n =~ m/^\./s or $n =~ m/[\000-\055\/]/s;
		$e = length $dir ? "$dir/$n" : $n;
		$f = "$base/$e";
		if (-f $f)
		{
			PODLIST: foreach (@pod_search_list)
			{
				if ($e =~ m/[^.]+$_->[1]$/ and $e =~ s/(^|\/)$_->[0](\Q$n\E)$/$1$2/s)
				{
					$e =~ s/$_->[1]$//s;
					$e =~ s/\//::/gs;
					next unless open ($pf, '<', $f);
					while ($s = readline ($pf))
					{
						if ($s =~ m/^=(pod|head1)/s)
						{
							push (@$list, [$e, $f]) unless first {$_->[0] eq $e} @$list;
							close ($pf);
							last PODLIST;
						}
					}
					close ($pf);
				}
			}
		}
		elsif (-d $f)
		{
			pod_all_modules_r ($base, $e, $list);
		}
	}
	closedir ($d);
}

sub pod_all_modules
{
	my @list;
	pod_all_modules_r ($_, '', \@list) foreach (@$inc);
	@list = sort {$a->[0] cmp $b->[0]} @list;
	return \@list;
}

sub pod_fts_refresh
{
	state ($fts_cache, $index_cache);
	return unless $config{'cache.int.dir'};
	return ($fts_cache, $index_cache) if $fts_cache;
	$fts_cache = $config{'cache.int.dir'} . '/' .
		 encode_filename ($perl_version) . ".fts";
	$index_cache = $config{'cache.int.dir'} . '/' .
		 encode_filename ($perl_version) . ".idx";
	my @stat;
	return ($fts_cache, $index_cache)
		 if (@stat = stat ($fts_cache)) and
		 $t - $stat[9] < ($config{'cache.search.max'} // 3600);

	my $log_fd;
	my $log = $config{'cache.int.dir'} . '/' .
		 encode_filename ($perl_version) . ".log";
	if (open ($log_fd, '<', $log))
	{
		my $pid = readline ($log_fd);
		unless (looks_like_number ($pid))
		{
			print (STDERR "$log: first line is not a number");
			die "FTS log file is corrupted.\n";
		}
		if (kill (0, $pid))
		{
			return ($fts_cache, $index_cache) if @stat;
			die "Full-text search cache is still being generated.\n";
		}
		else
		{
			local $/;
			my $error = readline ($log_fd) // '';
			die "FTS process ended with error:\n$error\n" if $error =~ m/[^\s]/s;
			print (STDERR "FTS process died; remove $log to restart");
			die "FTS process died.\n";
		}
	}
	else
	{
		die "Cannot open FTS log file: $!\n" unless $!{ENOENT};
		close ($log_fd);
		my $pid = fork ();
		if (not defined $pid)
		{
			print (STDERR "Cannot fork: $!\n");
			return;
		}
		elsif ($pid)
		{
			return ($fts_cache, $index_cache) if @stat;
			die 'The process of building full-text cache for the first time ' .
				 "has just started.\n"
		}
	}

	my ($f, $i);
	my $fts_tmp = "$fts_cache.tmp";
	my $index_tmp = "$index_cache.tmp";
	unless (open ($f, '>>:utf8', $fts_tmp))
	{
		print (STDERR "Cannot create temporary file '$fts_tmp': $!\n");
		exit (0);
	}
	if ($config{'search.index'} and not open ($i, '>>:utf8', $index_tmp))
	{
		print (STDERR "Cannot create temporary file '$index_tmp': $!\n");
		exit (0);
	}
	$SIG{HUP} = 'IGNORE';

	exit (0) unless flock ($f, LOCK_EX | LOCK_NB) and
		 seek ($f, 0, SEEK_SET) and truncate ($f, 0);
	open ($log_fd, '>', $log) or die "Cannot open log file '$log': $!\n";
	print ($log_fd "$$\n");
	open (*STDIN, '<', '/dev/null');
	open (*STDOUT, '>&', $log_fd);
	open (*STDERR, '>&', $log_fd);
	close ($log_fd);

	if ($config{'search.index'})
	{
		exit (0) unless seek ($i, 0, SEEK_SET) and truncate ($i, 0);
	}

	my $modules = pod_all_modules ();
	my $m;
	foreach $m (@$modules)
	{
		print ($f "$m->[0]\n", $config{'search.all'} ? '' : "\n\n");
		next unless $config{'search.all'} or $config{'search.index'};
		my $parser = Pod::Simple::SimpleTree->new ();
		$parser->accept_targets ('html');
		my $pod = $parser->parse_file ($m->[1]);
		if ($pod and $pod->{root})
		{
			if ($config{'search.all'})
			{
				my $text = pod_text ($pod->{root});
				my $abstract = pod_abstract ($pod->{root});
				$abstract =~ s/[\r\n\s]+|(?:<br>)+/ /gs;
				$abstract =~ s/^\s*\Q$m->[0]\E\s*[-–—―‒]?\s*//i;
				my $limit = $config{'search.abstract.max'} // 300;
				if (length ($abstract) > $limit)
				{
					$abstract =~ s/<.*?(>|$)//g;
					substr ($abstract, $limit, length ($abstract), '')
						 if length ($abstract) > $limit;
					my $ls = rindex ($abstract, ' ');
					substr ($abstract, $ls, 20, '') if $limit - $ls < 20;
					$abstract =~ s/(&[a-z]+)?\s*$/.../;
				}
				print ($f $abstract, "\n", ($text =~ s/[\r\n\s]+/ /grs), "\n");
			}
			if ($config{'search.index'} and ($m->[0] ne 'perltoc' or
				 $config{'search.index.perltoc'} // 1))
			{
				my $index = pod_index_scan ($pod->{root});
				print ($i $_->[3] =~ s/[\r\n\s]+/ /grs, "\n", $m->[0], "\n",
					 $_->[0], "\n", $_->[2], "\n", $_->[1] =~ s/[\r\n\s]+/ /grs, "\n")
					 foreach (@$index);
			}
		}
	}

	rename ($fts_tmp, $fts_cache);
	close ($f);
	if ($i)
	{
		rename ($index_tmp, $index_cache);
		close ($i);
	}
	unlink ($log);
	exit (0);
}

sub pod_fts_cache
{
	my $search = shift;
	my ($fts_cache, $index_cache) = pod_fts_refresh ();
	return unless $fts_cache;
	my $f;
	unless (open ($f, '<:utf8', $fts_cache))
	{
		my $e = $!;
		print (STDERR "Cannot open full-text search cache '$fts_cache': $e\n");
		die "Cannot open the full-text search cache file ($e).\n";
	}

	my (@found_names, @found_content);
	my ($module, $abstract, $text, $p, $b);
	my ($before, $after) = ($config{'search.all.before.num'} // 100,
		 $config{'search.all.after.num'} // 100);
	my $lcs = lc ($search);
	my $search_content = $config{'search.all'};
	while (defined $f and $module = readline ($f) and
		 $abstract = readline ($f) and $text = readline ($f))
	{
		chomp ($module, $abstract, $text);
		push (@found_names, [$module, $abstract]) if $module =~ m/\Q$search\E/is;
		if (length $search and $search_content and
			 ($p = index (lc ($text), $lcs)) != -1)
		{
			$b = $before > $p ? $p : $before;
			$text = substr ($text, $p - $b, $b + length ($lcs) + $after);
			$text =~ m/ (.*)(\Q$search\E)(.*) /is or
				 $text =~ m/(.*)(\Q$search\E)(.*)/is;
			push (@found_content, [$module,
				 ($b < $before ? '' : '…') . encode_html ($1) .
				 "<b>" . encode_html ($2) . "</b>" .
				 encode_html ($3)]);
		}
	}
	close ($f);
	return (\@found_names, \@found_content);
}

sub pod_index_cache
{
	my $search = shift;
	my ($fts_cache, $index_cache) = pod_fts_refresh ();
	return unless $index_cache;
	my $f;
	unless (open ($f, '<:utf8', $index_cache))
	{
		print (STDERR "Cannot open index cache '$index_cache': $!\n");
		return;
	}

	my (@index_cache, $stext, $module, $id, $section, $label);
	while (defined $f and $stext = readline ($f) and $module = readline ($f) and
		 $id = readline ($f) and $section = readline ($f) and
		 $label = readline ($f))
	{
		chomp ($module, $id, $section, $label);
		push (@index_cache, [$module, $id, $section, $label])
			 if ($stext =~ m/\Q$search\E/i);
	}
	close ($f);
	@index_cache = sort {($a->[3] cmp $b->[3]) || ($a->[0] cmp $b->[0])}
		 @index_cache;
	return \@index_cache;
}


sub syntax_sourcehighlight
{
	my ($syntax, $text) = @_;
	state ($sh, $lm);
	my $lang;

	unless ($sh)
	{
		require Syntax::SourceHighlight;
		$sh = Syntax::SourceHighlight::SourceHighlight->new ('htmlcss.outlang');
		$lm = Syntax::SourceHighlight::LangMap->new ();
		$sh->setGenerateVersion (0);
		$sh->setBinaryOutput (1);
	}

	$lang = $lm->getMappedFileName ($syntax ? $syntax : 'perl');
	return undef unless $lang;
	return "\n" .
		 $sh->highlightString ($text, $lang) =~ s/<!--.*?-->|<\/?tt>//grs;
}


sub syntax_perltidy
{
	my ($syntax, $text) = @_;
	return undef if length ($syntax // '') and $syntax ne 'perl';

	require Perl::Tidy;
	my $o = '';
	return undef if Perl::Tidy::perltidy (source => \$text,
		 destination => \$o, argv => '-html -q');

	$o =~ s/^.*?\n<pre>\n*/\n<pre>/s;
	$o =~ s/\s*<\/body>\s*<\/html>\s*//s;
	return $o;
}


eval { read_file ($config{'page.html'} // 'page.html', \$output); };
fail ($@) if $@;

$search = $data{s};
$search = $config{'page.default'} // 'perl'
	 unless defined $search and length $search;
if (defined $config{'cache.int.dir'})
{
	$cache_dir = $config{'cache.int.dir'};
	unless (-d $cache_dir or (mkdir ($cache_dir) and chmod (0700, $cache_dir)))
	{
		print (STDERR "Cannot create cache directory '$cache_dir': $!\n");
		undef $cache_dir;
	}
	$cache_dir .= '/' . encode_filename ($perl_version) . '.pc/' if $cache_dir;
}

$data{site_css} = '<link rel="Stylesheet" type="text/css" href="site.css">'
	 if -f 'site.css';
$data{opt_default} = $config{'string.opt.default'} //
	 "default ($data{version})";



sub render
{
	my $provider = shift;
	eval { &$provider(); };

	if ($@)
	{
		$data{module} = $config{'string.error.title'} // 'Error';
		$data{name} = $config{'string.error.name'} // 'Page rendering error';
		$data{content} = '
			 <div class="error">
				 ' . sprintf ($config{'string.error.text'} //
				'Cannot get the information you requested.<br><br>
				The error was:<br>
				%s', encode_html ($@)) . '
			</div>';
	}
	else
	{
		my $last_level = 0;
		my $toc;
		foreach (@id_list)
		{
			if ($_->{level} and $_->{level} =~ m/^[0-9]+$/s
				 and $_->{level} > 0 and $_->{level} < '5')
			{
				while ($last_level < $_->{level})
				{
					$last_level++;
					$toc .= ('	' x $last_level) . "<ul>\n";
				}
				while ($last_level > $_->{level})
				{
					$toc .= ('	' x $last_level) . "</ul>\n";
					$last_level--;
				}
				$toc .=
					 ('	' x ($last_level + 1)) .
					 '<li><a href="#' . encode_html($_->{id}) . '">' . $_->{text} .
					 '</a>' . "\n";
			}
		}
		while ($last_level)
		{
			$toc .= ('	' x $last_level) . "</ul>\n";
			$last_level--;
		}
		$data{content} = "\n<div class=\"toc\">\n$toc</div>\n\n" . $data{content}
			 if $toc;

		$data{content} = "\n<div class=\"labels\">\n" .
			 join ('', map {"\t<span>$_</span>\n"} @{ $data{labels} }) .
			 "</div>\n$data{content}"
			if $data{labels}
	}

	$data{date} = strftime ('%Y-%m-%d %H:%M', localtime $t);
	$data{rtime} = sprintf ($config{'string.rtime'} // 'rendered in %.2fs',
		 Time::HiRes::time() - $t) unless $data{rtime};
	$data{module} =~ s/[\000-\037]//gs;
	if (length $data{title})
	{
		$data{title_dash} = '—';
		$data{title} =~ s/^\s*$data{module}\s*[-–—―‒]\s*//s;
		$data{title} =~ s/[\000-\037]//gs;
	}
	$data{logo} = $config{'string.logo'} //
		 '<object data="logo.svg" type="image/svg+xml"></object><br>POD Web';

	$data{extra_links} = '';
	$data{extra_links} .= '<a href="browse.cgi' . encode_html (query_make ()) .
		 '">a-z</a>'
		 if $config{'cache.int.dir'} and $config{'search.all'};

	if ($cache_file)
	{
		unlink($cache_file) if -f $cache_file;
		my $f;
		my $dir = $cache_file =~ s/\/[^\/]*$//rs;
		if ((-d $dir or mkdir ($dir, 0770)) and open ($f, '>:utf8', $cache_file))
		{
			print ($f $data{module}, "\n", $data{module_version} // '', "\n",
				 $data{title}, "\n", $data{content});
			close ($f);
			chmod (0440, $cache_file);
		}
	}

	$output =~ s/\$\{(#?)([a-z_]+)\}/
		 $1 ? encode_html($data{$2}\/\/'') : $data{$2}\/\/''/egs;
	print (
			 "Content-type: text/html; charset=utf-8\r\n" .
			 "Content-length: " . length(encode('UTF-8', $output)) . "\r\n" .
			 "Cache-Control: max-age=" . ($config{'cache.age.max'} // 300) . "\r\n" .
			 "\r\n",
			 $output
		 );
	return ();
}

1;

__END__

=encoding UTF-8

=for html
<div style="height: 0px;"><object data="knight.svg" type="image/svg+xml" style="position: absolute; top: 6em; right: 3em; height: 8em;"></object></div>

=head1 NAME

B<PodWEB> - a simple CGI script displaying L<P.O. Documents|perlpod>.

=head1 DESCRIPTION

This simple script is similar in function to the well-known
L<perldoc.perl.org|https://perldoc.perl.org/> or
L<perldoc.pl|https://perldoc.pl/> while adhering to the K.I.S.S. 
(I<keep it simple, soldier>) principle. Assuming that one has a working
HTTPd/CGI environment, the deployment is as simple as copying the repository to
the destination directory and/or adjusting the configuration file. It runs in a
bare Perl 5.20+ environment.

The script accepts two variables from the URL string:

=over

=item B<v> (version)

indicates the library path where L<PODs|perlpod> are taken from; if empty, then
the currently running Perl L<include path|perlvar/@INC> is considered;

=item B<s> (search)

is the search keyword; if it resembles a module name, then it is looked for in
the version-dependent search path; otherwise, a configurable search through the
documents is performed.

=back

=head2 Searching

PodWEB performs these steps whenever a page is searched for through the use of
the search box:

=over

=item 1

Module by that name is looked up and its POD content is displayed. This is
similar in function to a plain L<perldoc> call.

=item 2

If L<function search|/search.perlfunc> is enabled, then the string is looked
up in L<perlfunc's|perlfunc> index and the browser is redirected directly to
the matching tag. Prepending the query with a question mark (C<?>) will cause
PodWEB to start from this step.

=item 3

If the L<built-in variable search|/search.perlvar> is enabled, then the string
is looked for in L<perlvar's|perlvar> index and the matching tag is displayed.

=item 4

If L<module name search|/search.names> is enabled, then the matching parts of
all module names are reported. This is the first stage when the results page
is shown instead of redirecting the user to a POD. Prepending the query with
a double question mark (C<??>) makes PodWEB start from here and always display
the search results page.

=item 5

If the L<module index search|/search.index> is enabled, then any matching index
entries (C<< XZ<><> >> tags) are shown.

=item 6

If L<text search|/search.all> is enabled, then all POD content is scanned. The
matching modules are shown along with part of the paragraph where the phrase
is found.

=back

=head2 The browser

Enabling the L<text search|/search.all> activates the module browser. It is
similar to perldoc's I<Core modules> section except that it displays all
modules available within the include path.

The browser can be reached by clicking the C<a-z> button in the search bar.

It accepts two variables from the query string – the Perl version/include path
(C<v>) and the first letter the module name starts with (C<p>).

=head2 Table support

PodWEB supports rendering tables formatted similarly to the L<psql(1)> output
with two enhancements:

=over

=item

multicolumn cells are also supported by simply skipping the column mark
C<|>,

=item

multiple separator lines may be included.

=back

It is also capable of setting character-based alignment through the
L<text-align with character|https://developer.mozilla.org/en-US/docs/Web/CSS/text-align>
CSS property although there is no browser supporting this.

Tables must be put in a verbatim (indented) block. Note that the separator
line is one character longer than the content on both sides.

The following code:

=for syntax none

    Header  | Header  | Header
   ---------+---------+---------
    Content | Content | Content
    Multicolumn       | Content
    Multi  +| Content | Content
    row    +|         |
    content |         |
   (3 rows)
   Time: 1 s

creates the table:

    Header  | Header  | Header
   ---------+---------+---------
    Content | Content | Content
    Multicolumn       | Content
    Multi  +| Content | Content
    row    +|         |
    content |         |
   (3 rows)
   Time: 1 s

=head2 Syntax highlighting

If a L<syntax highlighting engine|/syntax.engine> is turned on, then the POD
renderer will process any verbatim blocks that look like Perl code with that
library. The non-standard C<< =for syntax I<language> >> command enables the
given language for the next verbatim block. For example:

=for syntax none

    =for syntax sql
    
        SELECT * FROM dual;

highlights SQL keywords:

=for syntax sql

    SELECT * FROM dual;

The C<=for syntax none> command turns syntax rendering off.

=head1 F<config>

The file called F<config> contains the following settings:

=over

=item B<query.length.max>

The maximum length of a request query item. The default is 128 characters and
that corresponds to the I<maxlength> value for the search input element in the
default HTML template.

=item B<page.default>

The page that is displayed when the search box is empty. The default is
I<perl>.

=item B<page.html>

The name of the template file to be used for L<page rendering|/HTML SOURCE>.
Defaults to F<page.html>.

=item B<cache.age.max>

This setting controls the C<Cache-Control> HTTP header. It adjusts the maximum
cache time (in seconds) at the proxy/browser side. The default is 300 seconds
(5 minutes).

=item B<cache.int.dir>

Internal cache dir. If set, then the rendered pages and content aggregation
caches are saved in that directory. This option is required for advanced search
functions to work. There is no default value here: the pages are not cached
until you add this setting to the configuration.

=item B<cache.int.max>

Internal cache expiry time in seconds. The cached content becomes invalid after
that time and will be rendered again. The default is 600 seconds.

=item B<cache.search.max>

Full-text search cache expiry time in seconds. The cache regeneration process
will be launched after that time. The default value is 3600 (1 hour).

=item B<search.perlfunc>

Whether the search through L<perlfunc> should be done if no module is found.
The search will look for the given phrase in the index entries and redirect
directly to that tag. This is turned off by default.

=item B<search.perlvar>

Enables searching for the given string in L<perlvar's index|perlvar> if no
module or no L<perlfunc> tag by that name is found. The user will be
automatically redirected if a matching index key is found. Default is off.

=item B<search.names>

Search for parts of module names. The text displayed depends on the number of
names found. The user will be presented with either a CPAN search link or a
number of options. Requires B<< L</cache.int.dir> >> to work. Default is off.

=item B<search.index>

This switch enables the generation and searching in the indexes of the POD
pages in the include path. Requires B<< L</cache.int.dir> >> to work. The index
keys are acquired from the C<< XZ<><> >> tags. Default is off.

=item B<search.all>

This enables the generation and searching in the textual content of all POD
pages in the search path. It will display the links to the pages and
configurable amount of the text below. Requires B<< L</cache.int.dir> >> to
work. It is turned off by default.

=item B<search.all.before.num>

The number of characters ahead of search phrase shown in the text search
results. The default is 100 characters.

=item B<search.all.after.num>

The number of characters after the search phrase shown in the results. The
default is 100 characters.

=item B<search.abstract.max>

The abstract line (the contents of the I<HEAD> section) is shown in the browser
next to the module name. It is supposed to be a short description of the
package, however, some creators put a lot more than it is necessary for there.

The default limit is 300 characters.

=item B<syntax.engine>

Turn on syntax highlighting. The argument selects the highlighting engine:

=over

=item C<perltidy>

uses the L<Perl::Tidy> module which is the basis of the perltidy utility
(only Perl syntax is supported),

=item C<sourcehighlight>

uses L<Syntax::SourceHighlight> library.

=back

Highlighting is turned off by default.

=item B<string.rtime>

Text to be displayed in place of C<${rtime}> for newly rendered pages. The
default is I<“rendered in ..s”>.

=item B<string.cached>

Text displayed in place of C<${rtime}> for cached pages. The default is
I<“retrieved from cache in ..s”>.

=item B<string.error.title>

Page title when an error is detected. The default is simply I<Error>.

=item B<string.error.name>

The description of the error page. The default is I<Page rendering error>.

=item B<string.error.text>

The actual error text on the page. C<%s> is replaced with the exception thrown
by the renderer. The default value is
I<< “Cannot get the information you requested. The error was: C<%s>” >>.

=item B<string.search.title>

Search page title. The C<%s> tag is replaced with the searched phrase. The
default value is I<Search results>.

=item B<string.search.name>

Search page description. C<%s> is replaced with the searched phrase. The
default is simply I<< C<%s> >>.

=item B<string.search.notfound>

The text that is displayed when nothing is found. C<%s> is replaced with the
search string. The default is header B<< I<Not found> >> followed by the text saying
I<The phrase ... you were looking for could not be found.>

=item B<string.search.names>

The text displayed before the list of module names found. The default value is
the header titled B<< I<Modules and POD pages> >>.

=item B<string.search.dym>

The text displayed if exactly one module is found. The first C<%s> is replaced
with the link target to the module found. The second is the module name itself.
The default text sais I<Did you mean ...?>

=item B<string.search.nomodules>

The text displayed if no module names were found. The two C<%s> contain HTML
encoded and URL encoded search strings. By default, it contains a link
redirecting to L<meta::cpan|https://metacpan.org> search –
I<No modules or POD pages found. Search for ... at meta::cpan instead.>

=item B<string.search.fts>

Full-text search header. The default value is header named
B<< I<Text search> >>.

=item B<string.search.index>

The text displayed before index search results. The default for this is simply
B<< I<Index> >>.

=item B<string.opt.default>

The text displayed in the unmodified F<page.html> as the default Perl version.
Normally it is I<default (version)>.

=item B<string.logo>

The logo displayed in the top-left corner of the page. Defaults to the PodWEB
SVG logo and some text below.

=item B<link.in_to>

Whether the subsection links be appended with POD page name. It is enabled by
default which follows perldoc's behaviour. For example,
C<< LZ<><Module/Section> >> will be rendered as I<Section in Module> when
turned on, and just I<Section> when off.

=item B<link.man>

The link for the manual pages. The first C<%s> is man section number and the
second is the name of the man page. By default, all man requests go to
L<< I<linux.die.net/man>|https://linux.die.net/man/ >>.

=item B<path.>I<perl version>

The C<path.*> options contain include paths for Perl versions or POD sources
available for browsing. Apart from this, the running Perl include path will
also be available as I<default> and it can also be redefined.

=back

=head1 HTML SOURCE

The F<page.html> file contains the HTML skeleton used to render the page.
Variable parts of the document are marked with the dollar sign and curly
brackets (i.e. C<${content}>.)

All variables coming from the query string are available in addition to the
following:

=over

=item B<< C<${module}> >>

indicates the module name or subpage that is loaded,

=item B<< C<${module_version}> >>

the version number extracted from C<our $VERSION> or similar statement in
the module,

=item B<< C<${title}> >>

document summary, the first paragraph of the I<NAME> section,

=item B<< C<${title_dash}> >>

expands to the long dash character (—) if the title is defined,

=item B<< C<${version}> >>

current running Perl version,

=item B<< C<${options}> >>

the selection of L<defined sets|/path.perl version> of documents,

=item B<< C<${logo}> >>

the contents of the C<< L</string.logo> >> configuration value,

=item B<< C<${extra_links}> >>

additional links at the top of the page; there is only one at the moment, the
module browser,

=item B<< C<${content}> >>

the generated POD content,

=item B<< C<${date}> >>

current date and time,

=item B<< C<${rtime}> >>

rendering time,

=item B<< C<${site_css}> >>

if the F<site.css> file is found in the working directory, then this variable
is filled with HTML stylesheet link element.

=back

If the variable name is prepended with the C<#> character, then its content is
quoted with HTML meta tags. For example:

    <input name="s" value="${#s}" maxlength="100">

quotes the user-supplied string I<s> rendering the exact text in the browser.

=head1 AUTHOR

Originally written and maintained by Matlib
(L<matlib@matlibhax.com|mailto:matlib@matlibhax.com>), any comments or
suggestions are welcome.

=head1 LICENCE

THIS SOFTWARE IS IN PUBLIC DOMAIN

There is no limitation on any kind of usage of this code. You use it because it
is yours.
