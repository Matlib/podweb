*PodWEB* - a simple CGI script displaying P.O. Documents.

# Description

This simple CGI script is similar in function to the well-known [perldoc.perl.org](https://perldoc.perl.org/) or [perldoc.pl](https://perldoc.pl/) while adhering to the K.I.S.S. (keep it simple, soldier) principle. Assuming that one has a working HTTPd/CGI environment, the deployment is as simple as copying the repository to the destination directory and/or adjusting the configuration file. It runs in a bare Perl 5.20+ environment.

It accepts two variables from the URL string:

* v (version)
  indicates the library path where PODs are taken from; if empty, then currently running Perl include path is considered;

* s (search)
  is the search keyword; if it resembles module name, then it is looked for in the version-dependent search path; otherwise a configurable search through the documents is performed.

# Author

Originally written and maintained by [Matlib](mailto:matlib@matlibhax.com). The demo page is available at [matlibhax.com/~matlib/hacks/podweb](https://matlibhax.com/~matlib/hacks/podweb/).
