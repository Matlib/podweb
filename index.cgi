#!/usr/bin/env perl

use strict;
use warnings;
no warnings 'redundant';
use utf8;

require './PodWEB.pm';
PodWEB->import();
our ($VERSION, %config, %data, $output, $cache_file, $cache_dir, @id_list,
	 $search, $t);

render (sub {
	my ($file, @stat);
	$cache_file .= $cache_dir . encode_filename ($search) if $cache_dir;

	if ($cache_file and @stat = stat ($cache_file) and
		 $t - $stat[9] < ($config{'cache.int.max'} // 600))
	{
		read_file ($cache_file, \$data{content});
		$data{content} =~ m/^(.*?)\n(.*?)\n(.*?)\n/s;
		($data{module}, $data{module_version}, $data{title}) = ($1, $2, $3);
		substr ($data{content}, 0, $+[0], '');
		$data{rtime} = sprintf ($config{'string.cached'} //
			 'retrieved from cache in %.2fs', Time::HiRes::time() - $t);
		undef $cache_file;
		return;
	}

	if ($search =~
		 m/^[^\/\?:.\s\r\n][^\/\?:\s\r\n]*(?:::[^\/\?:.\s\r\n][^\/\?:\s\r\n]*)*$/s)
	{
		($file, $data{module}, $data{module_version}) = pod_search ($search);
	}

	if (defined $file)
	{
		my $parser = Pod::Simple::SimpleTree->new ();
		$parser->accept_targets ('html', 'syntax');
		my $pod = $parser->parse_file ($file);

		require Module::CoreList;
		require version;
		if (my $cv = Module::CoreList->first_release($search))
		{
			push (@{ $data{labels} }, 'Core module<br>since <b>' .
				 encode_html (version->parse ($cv)->normal ()) . '</b>');
			push (@{ $data{labels} },
				 '<span class="red">Deprecated</span><br>since <b>' .
				 encode_html (version->parse ($cv)->normal ()) . '</b>')
				if $cv = eval { Module::CoreList->deprecated_in($search) };
			push (@{ $data{labels} },
				 '<span class="red">Removed</span><br>from <b>' .
				 encode_html (version->parse ($cv)->normal ()) . '</b>')
				if $cv = eval { Module::CoreList->removed_from($search) };
		}

		pod_render ($pod->{root}, \$data{content}, 1, \@id_list);
	}

	else
	{
		$search =~ s/^\s*(\?\??)\s*//s;

		unless ($1 and $1 eq '??')
		{
			if ($config{'search.perlfunc'} and ($file = pod_search ('perlfunc')))
			{
				my $func;
				read_file ($file, \$func);
				redirect (query_make (s => 'perlfunc#' . pod_id_tidy ($search)))
					 if $func =~ m/X<\Q$search\E>|X<< \Q$search\E >>/;
			}
			if ($config{'search.perlvar'} and ($file = pod_search ('perlvar')))
			{
				my $var;
				read_file ($file, \$var);
				redirect (query_make (s => 'perlvar#' . pod_id_tidy ($search)))
					 if $var =~ m/X<\Q$search\E>|X<< \Q$search\E >>/;
			}
		}

		my ($found_names, $found_content, $found_index);
		($found_names, $found_content) = pod_fts_cache ($search)
			 if $config{'search.names'} or $config{'search.all'};

		if ($config{'search.names'})
		{
			$data{content} .= "\n" . ($config{'string.search.names'} //
				 '<h1>Modules and POD pages</h1>') . "\n\n";
			if ($found_names and @$found_names)
			{
				if (@$found_names == 1)
				{
					$data{content} .=
						 sprintf (($config{'string.search.dym'} //
						 '<p>Did you mean <a href="%s">%s</a>?') . "\n",
						 encode_html (query_make (s => $found_names->[0][0])),
						 encode_html ($found_names->[0][0]));
				}
				else
				{
					my ($module, $abstract);
					foreach (@$found_names)
					{
						($module, $abstract) = @$_;
						$module =~ m/(.*)(\Q$search\E)(.*)/is;
						$data{content} .=
							 '<div class="sresult"><a href="' .
							 encode_html (query_make (s => $module)) . '">' .
							 encode_html ($1) .
							 '<b>' . encode_html ($2) . '</b>' .
							 encode_html ($3) .
							 '</a>' .
							 # no encode_html - $abstract comes from pod_text_markup()
							 (length ($abstract) ? " – $abstract" : '') .
							 "</div>\n";
					}
				}
			}
			else
			{
				$data{content} .=
					 sprintf (($config{'string.search.nomodules'} //
					 '<p>No modules or POD pages found. Search for <i>%s</i> ' .
					 'at <a href="https://metacpan.org/search?q=%s">' .
					 '<tt style="color: #43424a;">meta' .
					 '<span style="font-weight: bold; color: #da2037;">::</span>cpan</tt>' .
					 '</a> instead.') . "\n",
					 encode_html ($search), encode_url ($search));
			}
		}

		if ($config{'search.index'})
		{
			my $found_index = pod_index_cache ($search);
			if ($found_index and @$found_index)
			{
				$data{content} .= "\n" . ($config{'string.search.index'} //
					 '<h1>Index</h1>') . "\n\n";
				my ($last_entry, $div, $links) = ('', 0, 0);
				my ($module, $id, $section, $label);
				foreach (@$found_index)
				{
					($module, $id, $section, $label) = @$_;
					if ($last_entry ne $label)
					{
						$data{content} .= "<br><br></div>\n" if $div;
						$data{content} .= '<div class="sresult">' . $label . '<br>';
						$div = 1;
						$links = 0;
						$last_entry = $label;
					}
					$data{content} .=
						 ($links ? '<br>' : '') .
						 encode_html ($module) . ' – ' .
						 '<a href="' . encode_html (query_make (s => $module)) .
						 '#' . encode_html ($id) . '">' .
						 $section . '</a>';
					$links++;
				}
				$data{content} .= "</div>\n" if $div;
			}
		}

		if ($config{'search.all'} and $found_content and @$found_content)
		{
			$data{content} .= "\n" . ($config{'string.search.fts'} //
				 '<h1>Text search</h1>') . "\n\n";
			foreach (@$found_content)
			{
				$data{content} .=
					 "<div class=\"sresult\">\n" .
					 "	<a href=\"" .
					 encode_html (query_make (s => encode_url ($_->[0]))) . "\">" .
					 encode_html ($_->[0]) . "</a><br>\n" .
					 "	$_->[1]<br><br>\n" .
					 "</div>\n";
			}
		}

		my $search_html = encode_html ($search);
		$data{module} = sprintf ($config{'string.search.title'} //
			 'Search results', $search_html);
		$data{name} = sprintf ($config{'string.search.name'} // '%s',
			 $search_html);
		$data{content} = sprintf ($config{'string.search.notfound'} //
			 "<h1>Not found</h1>\n" .
			 "<p>The phrase <i>%s</i> you were looking for could not be found.",
			 $search_html) unless $data{content};
	}
});
